Color.Silver = Color('C0C0C0')
function MenuNodeMainGui:_setup_item_rows( node )
	MenuNodeMainGui.super._setup_item_rows( self, node )
	if alive( self._version_string ) then
		self._version_string:parent():remove( self._version_string )
		self._version_string = nil
	end
	self._version_string = self.ws:panel():text( { name = "version_string", text = "1.52.2 正體中文化", font = tweak_data.menu.pd2_small_font, font_size = 15, color = Color.Silver, align = SystemInfo:platform() == Idstring( "WIN32" ) and "right" or "left", vertical = "bottom", alpha = 0.5 } )
end