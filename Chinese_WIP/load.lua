---Do Not Touch Anything Below,unless you're a professtional! ...Okay i'm not a profestional either.

if string.lower(RequiredScript) == "lib/managers/localizationmanager" then

if not TCTranslate then

if not _G.ChineseWIP then
	_G.ChineseWIP = {}
	ChineseWIP.mod_path = ModPath
end

Hooks:Add("LocalizationManagerPostInit", "LocalizationManagerPostInit_TradChinese", function(Localization)

	local texts = {}

	local file_names = {ChineseWIP.mod_path .."Localization/tchinese.lua",}

	for _,filename in pairs(file_names or {})do
		local fo = io.open(filename,"r")
		if fo then
			local txt = fo:read("*a")
				io.close(fo)
			if txt and type(txt) == "string" then
				local data = assert(loadstring("local text = {\n"..txt.."\n}return text"))()
				if data and type(data) == "table" then
					for i,v in pairs(data)do
						if v ~= "" then
							texts[i] = v
						end
					end
				end
			end
		end
	end
	Localization:add_localized_strings(texts)
end
)

function LocalizationManager.text( self, str, macros )
	if self._custom_localizations[str] then
		local return_str = self._custom_localizations[str]
		if macros and type(macros) == "table" then
			for k, v in pairs( macros ) do
				return_str = return_str:gsub("$" .. k .. ";","$" .. k .. ";")
			end
		end
		self._macro_context = macros
		return_str = self:_localizer_post_process( return_str )
		self._macro_context = nil
		return return_str
	end
	return self.orig.text(self, str, macros)

end
	
ModPath = mods/Chinese_WIP

end
end

if string.lower(RequiredScript) == "lib/units/props/timergui" then

    CloneClass(TimerGui)

    function TimerGui.init(this, unit)
       local res = this.orig.init(this, unit)
       local timer_header_text = managers.localization:text("prop_timer_gui_estimated_time")
           this._gui_script.time_header_text:set_text(timer_header_text)
       return res
    end
end

if string.lower(RequiredScript) == "lib/utils/levelloadingscreenguiscript" then

    LevelLoadingScreenGuiScript.old__init = LevelLoadingScreenGuiScript.old__init or LevelLoadingScreenGuiScript.init

    function LevelLoadingScreenGuiScript:init(scene_gui, res, progress, base_layer)
	    self:old__init(scene_gui, res, progress, base_layer)	
	    local title_text = "載入中"--managers.localization:text("debug_loading_level")
	    self._level_title_text:set_text(title_text)
    end
	
end

if string.lower(RequiredScript) == "lib/utils/lightloadingscreenguiscript" then

    LightLoadingScreenGuiScript.old__init = LightLoadingScreenGuiScript.old__init or LightLoadingScreenGuiScript.init

    function LightLoadingScreenGuiScript:init(scene_gui, res, progress, base_layer, is_win32)
	    self:old__init(scene_gui, res, progress, base_layer, is_win32)
	    self._title_text:set_text("載入中")--managers.localization:text("debug_loading_level")
    end
end